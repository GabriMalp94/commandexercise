﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootBehavior : MonoBehaviour
{
    [Header("Projectile")]
    [SerializeField] Vector3 pistolOffset;
    [SerializeField] GameObject projectilePrefab;
    [SerializeField] float projectileSpeed = 800f;
    [SerializeField] float projectileFiringPériod = 0.05f;

    public void Fire()
    {
        Vector2 mouseWorldPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        Vector2 direction = (mouseWorldPosition - (Vector2)transform.position).normalized;

        GameObject laser = Instantiate(projectilePrefab, transform.position + pistolOffset, Quaternion.identity) as GameObject;
        laser.GetComponent<Rigidbody2D>().velocity = direction * projectileSpeed * Time.deltaTime;
    }
}
