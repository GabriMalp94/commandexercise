﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{

    [Header("Player")]

    private PlayerMovement _playerMovement;
    private ShootBehavior _shootBehavior;

    ICommand w_command;
    ICommand s_command;
    ICommand d_command;
    ICommand a_command;
    ICommand fire_command;

    private void Awake()
    {
        _playerMovement = GetComponent<PlayerMovement>();
        _shootBehavior = GetComponent<ShootBehavior>();
        SetCommand();
    }


    private void SetCommand()
    {
        w_command = new Command(_playerMovement.MoveUp);
        s_command = new Command(_playerMovement.MoveDown);
        a_command = new Command(_playerMovement.MoveLeft);
        d_command = new Command(_playerMovement.MoveRight);
        fire_command = new Command(_shootBehavior.Fire);
       
    }

    void Update()
    {
        InputCheck();
    }

    private void InputCheck()
    {
        if(Input.GetKeyDown(KeyCode.W))
        {
            w_command.Execute();
        }
        if (Input.GetKeyDown(KeyCode.S))
        {
            s_command.Execute();
        }
        if (Input.GetKeyDown(KeyCode.D))
        {
            d_command.Execute();
        }
        if (Input.GetKeyDown(KeyCode.A))
        {
            a_command.Execute();
        }
        if(Input.GetKeyDown(KeyCode.Mouse0))
        {
            fire_command.Execute();
        }
    }

   
}
