﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public delegate void CommandDelegate();
public interface ICommand 
{
    void Execute();
}

public class Command : ICommand
{
    CommandDelegate _command;

    public Command(CommandDelegate command)
    {
        _command = command;
    }

    public void Execute()
    {
        _command();
    }
}